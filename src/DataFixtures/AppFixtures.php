<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\UserSession;
use App\Entity\Wall;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setLogin('login1');
        $user->setFirstName('Mark');
        $user->setLastName('Davis');
        $user->setPasswordHash(password_hash('password123',1));

        $wall = new Wall();
        $user->setWall($wall);

        $session = $user->createSession(new \DateInterval('PT15M'));

        $manager->persist($wall);
        $manager->persist($user);
        $manager->persist($session);

        $manager->flush();
    }
}
