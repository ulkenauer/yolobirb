<?php

namespace App\Repository;

use App\Entity\UserPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPost[]    findAll()
 * @method UserPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPost::class);
    }

    public function getAllPostsOfWall($value, $offset, $max)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.wall = :val')
            ->setParameter('val', $value)
            ->setFirstResult($offset)
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return UserPost[] Returns an array of UserPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPost
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
