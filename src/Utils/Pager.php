<?php

namespace App\Utils;


class Pager
{
    /**
     * @param string $page
     * @return bool|string
     */
    public function getPage(string $page)
    {

        /*$data = json_decode(file_get_contents(__DIR__.'/../../assets/pages.json'), true);

        return file_get_contents(__DIR__.'/../../assets/pages/'.$data[$page]);*/
        return file_get_contents(__DIR__.'/../../assets/pages/'.$page);
    }
}