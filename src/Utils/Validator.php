<?php

namespace App\Utils;


class Validator
{
    public function validateLogin(string $login): bool
    {
        return (strlen($login) <= 255 && strlen($login) > 3 && !preg_match('%[^a-zA-Z0-9]%', $login));
    }

    public function validatePassword(string $password): bool
    {
        return (strlen($password) <= 255 && strlen($password) > 8 && !preg_match('%[^a-zA-Z0-9]%', $password));
    }
}