<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181120052354 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cms_walls (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cms_user_posts ADD wallId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_user_posts ADD CONSTRAINT FK_595F3A2758EEE9D5 FOREIGN KEY (wallId) REFERENCES cms_walls (id)');
        $this->addSql('CREATE INDEX IDX_595F3A2758EEE9D5 ON cms_user_posts (wallId)');
        $this->addSql('ALTER TABLE cms_user_sessions CHANGE token token VARCHAR(64) NOT NULL');
        $this->addSql('ALTER TABLE cms_users ADD wallId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_users ADD CONSTRAINT FK_3AF03EC558EEE9D5 FOREIGN KEY (wallId) REFERENCES cms_walls (id)');
        $this->addSql('CREATE INDEX IDX_3AF03EC558EEE9D5 ON cms_users (wallId)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_user_posts DROP FOREIGN KEY FK_595F3A2758EEE9D5');
        $this->addSql('ALTER TABLE cms_users DROP FOREIGN KEY FK_3AF03EC558EEE9D5');
        $this->addSql('DROP TABLE cms_walls');
        $this->addSql('DROP INDEX IDX_595F3A2758EEE9D5 ON cms_user_posts');
        $this->addSql('ALTER TABLE cms_user_posts DROP wallId');
        $this->addSql('ALTER TABLE cms_user_sessions CHANGE token token VARCHAR(64) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('DROP INDEX IDX_3AF03EC558EEE9D5 ON cms_users');
        $this->addSql('ALTER TABLE cms_users DROP wallId');
    }
}
