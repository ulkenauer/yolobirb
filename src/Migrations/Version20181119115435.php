<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181119115435 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_user_posts ADD authorLogin VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_user_posts ADD CONSTRAINT FK_595F3A27D2031CD0 FOREIGN KEY (authorLogin) REFERENCES cms_users (login)');
        $this->addSql('CREATE INDEX IDX_595F3A27D2031CD0 ON cms_user_posts (authorLogin)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_user_posts DROP FOREIGN KEY FK_595F3A27D2031CD0');
        $this->addSql('DROP INDEX IDX_595F3A27D2031CD0 ON cms_user_posts');
        $this->addSql('ALTER TABLE cms_user_posts DROP authorLogin');
    }
}
