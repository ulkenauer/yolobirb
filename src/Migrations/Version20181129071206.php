<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181129071206 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_user_posts ADD time DATETIME DEFAULT NULL, CHANGE authorLogin authorLogin VARCHAR(255) NOT NULL, CHANGE wallId wallId INT NOT NULL');
        $this->addSql('ALTER TABLE cms_user_sessions CHANGE userLogin userLogin VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cms_user_posts DROP time, CHANGE authorLogin authorLogin VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE wallId wallId INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cms_user_sessions CHANGE userLogin userLogin VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
