<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @namespace App
 * @brief Base namespace
 */

/**
 * @namespace App::Entity
 * @brief Namespace for all model classes.
 *
 */

class User
{
    private $firstName;
    private $lastName;
    private $passwordHash;
    private $login;
    private $wall; //!< @brief the wall that belongs to user.

    public function createSession(\DateInterval $duration): UserSession
    {
        $session = new UserSession();
        $session->setUser($this);
        $session->setExpirationDate((new \DateTime())->add($duration));
        $session->generateToken();

        return $session;
    }

    public function verifyPassword(string $password): bool
    {
        return password_verify($password, $this->passwordHash);
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    public function setPasswordHash(string $passwordHash): self
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $newLogin)
    {
        $this->login = $newLogin;
    }

    public function getWall(): ?Wall
    {
        return $this->wall;
    }

    public function setWall(?Wall $wall): self
    {
        $this->wall = $wall;

        return $this;
    }
}
