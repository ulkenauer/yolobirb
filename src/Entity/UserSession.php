<?php

namespace App\Entity;


class UserSession
{
    private $token;
    private $user;
    private $expirationDate;

    public function expired(): bool
    {
        return ($this->expirationDate < new \DateTime());
    }

    public function getExpirationDate(): ?\DateTimeInterface
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(\DateTimeInterface $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    public function generateToken()
    {
        // todo: it is necessary to exclude the possibility of generating the same tokens
        $this->token = base64_encode(random_bytes(48));
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}