<?php

namespace App\Controller;
use App\Entity\UserPost;
use App\Utils\Pager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WallController extends AbstractController
{
    use SessionAwareControllerTrait;

    public function index(Pager $pager)
    {
        if($this->getSession() != null)
        {
            return new RedirectResponse('/');
        }

        return new Response($pager->getPage('wall.html'));
    }

    public function getWallPosts(LoggerInterface $logger)
    {
        $request = Request::CreateFromGlobals();
        $response = new Response();

        $session = $this->getSession();

        if($session == null)
        {
            $response->setStatusCode(401);
            return $response;
        }

        try
        {
            $data = json_decode($request->getContent(), true);
            $offset = $data['control']['offset'];
            $max = $data['control']['max'];
            $login = $data['login'];
        }
        catch (\Exception $exception)
        {
            $response->setStatusCode(400);
            return $response;
        }

        $repo = $this->getDoctrine()->getRepository('App\Entity\UserPost');
        $userRepo = $this->getDoctrine()->getRepository('App\Entity\User');
        //$user = $session->getUser();
        $user = $userRepo->find($login);

        if($user == null)
            return new Response('',404);

        $wall = $user->getWall();

        $posts = $repo->getAllPostsOfWall($wall,$offset,$max);
        $postsArray = array();
        foreach ($posts as $post)
        {
            array_push($postsArray,['title' => $post->getTitle(), 'text' => $post->getText(), 'author' => $post->getAuthor()->getLogin(), 'time' => $post->getTime()->format('Y-m-d H:i:s')]);
        }

        return $this->json($postsArray);

    }

    public function getWallId()
    {
        $response = new Response();

        $session = $this->getSession();

        if($session == null)
        {
            $response->setStatusCode(401);
            return $response;
        }

        $response->setContent($session->getUser()->getWall()->getId()); //todo: maybe change to json
    }

    public function addPost(LoggerInterface $logger)
    {
        $request = Request::CreateFromGlobals();
        $response = new Response();

        $session = $this->getSession();

        if($session == null)
        {
            $response->setStatusCode(401);
            return $response;
        }

        try
        {
            $data = json_decode($request->getContent(), true);
            $postText = $data['post']['text'];
            $postTitle = $data['post']['title'];
            $login = $data['login'];
        }
        catch (\Exception $exception)
        {
            $response->setStatusCode(400);
            return $response;
        }

        try
        {
            $repo = $this->getDoctrine()->getRepository('App\Entity\User');
        }
        catch (\Exception $exception)
        {
            $response->setStatusCode(400);
            return $response;
        }


        $user = $session->getUser();
        //$wall = $repo->find($wallId);
        //$wall = $user->getWall();
        $wall = $repo->find($login)->getWall();


        $post = new UserPost();
        $post->setAuthor($user);
        $post->setWall($wall); //todo: wall id should be obtained from client
        $post->setTime(new \DateTime());
        $post->setText($postText);
        $post->setTitle($postTitle);


        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();

        $response->setContent('Success');
        return $response;
    }

}