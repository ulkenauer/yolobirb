<?php

namespace App\Controller;


use App\Entity\User;
use App\Entity\UserSession;
use App\Entity\Wall;


use App\Utils\Pager;

use App\Utils\Validator;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;


class RegisterController extends AbstractController
{
    use SessionAwareControllerTrait;
    public function index(Pager $pager)
    {
        if($this->getSession() != null)
            return new RedirectResponse('/');
        return new Response($pager->getPage('RegisterPage.html'));
    }

    public function register(LoggerInterface $logger)
    {
        $response = new Response();

        if($this->getSession() != null)
            return $response;

        $request = Request::createFromGlobals();


        try
        {
            $data = json_decode($request->getContent(), true);
            $login = $data['Login'];
            $password = $data['Password'];
            $firstname = $data['Firstname'];
            $lastname = $data['Lastname'];
        }
        catch (\Exception $exception)
        {
            $response->setStatusCode(400);
            return $response;
        }



        $validator = new Validator();

        if(!$validator->validateLogin($login) || !$validator->validatePassword($password))
        {
            $response->setStatusCode(400);
            return $response;
        }

        $repository = $this->getDoctrine()->getRepository('App\Entity\User');
        if($repository->find($login) != null)
        {
            $response->setContent('User with that login already exists');
            return $response;
        }
        $manager = $this->getDoctrine()->getManager();
        $user = new User();

        $user->setLogin($login);
        $user->setFirstName($firstname);
        $user->setLastName($lastname);
        $user->setPasswordHash(password_hash($password,1));

        $wall = new Wall();
        $user->setWall($wall);

        //$session = $user->createSession(new \DateInterval('PT1H15M'));

        $logger->debug($wall->getId());
        $manager->persist($wall);
        $manager->persist($user);
        //$manager->persist($session);

        $manager->flush();
        //todo uncomment session creation
        //todo return cookies with session_key
        $response->setContent('Success');
        return $response;
    }
}