<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Trait SessionAwareControllerTrait
 * @package App\Controller
 * This trait should be used by any Controller
 * that wants to verify user identity.
 */
trait SessionAwareControllerTrait
{
    use ControllerTrait;

    /**
     * @return UserSession|null
     * If session_key passed by client, getUser()
     * will try to find session with that key, and its
     * user. If token is expired or there is no session_key -
     * null will be returned.
     */
    protected function getSession()
    {
        $request = Request::CreateFromGlobals();
        if($request->cookies->has('session_key'))
        {
            $sessionKey = $request->cookies->get('session_key');
            $session = $this->getDoctrine()->getRepository('App\Entity\UserSession')->find($sessionKey);
            if($session != null && !$session->expired())
            {
                return $session;
            }
        }
        return null;
    }
}