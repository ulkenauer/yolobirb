<?php

namespace App\Controller;

use App\Utils\Pager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IndexController
 * @package App\Controller
 */

class IndexController extends AbstractController
{
    use SessionAwareControllerTrait;

    /**
     * @param Pager $pager
     * @return Response
     */
    public function index(Pager $pager, LoggerInterface $logger)
    {

        $session = $this->getSession();
        if($session != null) {
            $login = $session->getUser()->getLogin();
            $url = $this->generateUrl('user', ['login' => $login]);//return new Response($pager->getPage('index.html')); //todo: change to redirect to postView
            return new RedirectResponse($url);
        }
        else
            return new Response($pager->getPage('greetings.html'));



        /*
                return $this->json([
                    'message' => 'Welcome to your new controller!',
                    'path' => 'src/Controller/IndexController.php',
                ]);*/
    }

    public function user(Pager $pager)
    {
        $session = $this->getSession();
        if($session != null) {
            return new Response($pager->getPage('index.html'));
        }
        else
        {
            $url = $this->generateUrl('index');
            return new RedirectResponse($url);
        }

    }
}
