<?php

namespace App\Controller;

use App\Utils\Pager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class LoginController
 * @package App\Controller
 */
class LoginController extends AbstractController
{
    use SessionAwareControllerTrait;
    /**
     * @param Pager $pager
     * @return RedirectResponse|Response
     */
    public function index(Pager $pager)
    {

        if($this->getSession() != null)
        {
            return new RedirectResponse('/');
        }

        return new Response($pager->getPage('LoginPage.html'));
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function login()
    {
        $request = Request::createFromGlobals();

        $response = new Response();

        if($this->getSession() != null)
        {
            return $response;
        }


        try
        {
            $data = json_decode($request->getContent(), true);
            $login = $data['Login'];
            $password = $data['Password'];
        }
        catch (\Exception $exception)
        {
            $response->setStatusCode(400);
            return $response;
        }

        $repo = $this->getDoctrine()->getRepository('App\Entity\User');
        $user = $repo->find($login);
        $status = "Failure";

        if($user != null && $user->verifyPassword($password))
        {
            $session = $user->createSession(new \DateInterval('PT1H15M'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush();
            $cookie = new Cookie('session_key',$session->getToken(),$session->getExpirationDate());
            $response->headers->setCookie($cookie);
            $status = "Success";
        }

        $response->setContent($status);

        return $response;
    }

    public function logout()
    {
        $response = new Response();
        $session = $this->getSession();
        if($session == null)
        {
            $response->setStatusCode(401);
            return $response;
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($session);
        $em->flush();

        return $response;
    }
}
