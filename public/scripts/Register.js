function updateStatusInfo(status)
{
    $("#status").text("Status: " + status);
    if(status === 'Success')
        window.location.href = '/';
}
function getPostData()
{
    var data =
        {
            "Login": $("#login").val(),
            "Password": $("#password").val(),
            "Lastname": $("#firstname").val(),
            "Firstname": $("#lastname").val()
        };
    return JSON.stringify(data);
}
function load()
{
    $.post("api/register", getPostData(), updateStatusInfo);
}
function onStart()
{
    document.getElementById("load").onclick = load;
}
$(document).ready(onStart);