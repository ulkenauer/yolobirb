
function updateInfo(status) {
    alert(status);
    if (status === 'Success')
        window.location.href = '/';
}

function main() {

    Vue.component('login-form',
        {
            data: function () {
                return { credentials: { Login: '', Password: '' } };
            }
            ,
            template: `
                    <span>
                    login <input v-model='credentials.Login'> </input>
                    <br>
                    password <input type='password' v-model='credentials.Password'> </input>
                    <br>
                    <button class="button" @click='redirectEvent'>Submit!</button>
                    </span>
                   `
            ,
            methods:
            {
                redirectEvent: function () {

                    this.$emit('sign-event', this.credentials);
                }
            }
        });

    Vue.component('register-form',
        {
            data: function () {
                return { input: { Login: '', Password: '', Password2: '', Firstname: '', Lastname: '' } };
            }
            ,
            template: `
                <div class="register-form">
                login <input v-model='input.Login'> </input>
                <br>
                firstname <input v-model='input.Firstname'> </input>
                <br>
                lastname <input v-model='input.Lastname'> </input>
                <br>
                password <input type='password' v-model='input.Password'> </input>
                <br>
                password <input type='password' v-model='input.Password2'> </input>
                <br>
                <button class="button" @click='redirectEvent'>Submit!</button>
                </div>
               `
            ,
            methods:
            {
                redirectEvent: function () {

                    if (this.input.Password === this.input.Password2) {
                        var credentials = {
                            Password: this.input.Password, Login: this.input.Login,
                            Firstname: this.input.Firstname, Lastname: this.input.Lastname
                        };

                        this.$emit('sign-event', credentials);
                    }
                    else {
                        alert('passwords are different');
                    }
                }
            }
        });


    new Vue({
        el: '#test2',
        methods:
        {
            foo: function (credentials) {
                $.post("api/login", JSON.stringify(credentials), updateInfo);
            }
        }
    })

    new Vue({
        el: '#test1',
        methods:
        {
            foo: function (credentials) {
                $.post("api/register", JSON.stringify(credentials), updateInfo);
            }
        }
    })

}
$(document).ready(main);