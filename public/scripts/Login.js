function updateStatusInfo(status)
{
    $("#status").text("Status: " + status);
    if(status === 'Success')
        window.location.href = '/';
}
function getPostData()
{
    var data =
        {
            "Login": $("#login").val(),
            "Password": $("#password").val()
        };
    return JSON.stringify(data);
}
function load()
{
    $.post("api/login", getPostData(), updateStatusInfo);
}
function onStart()
{
    document.getElementById("load").onclick = load; //this one works too
    //$("#load").click(load); //with jQuery
}
$(document).ready(onStart);