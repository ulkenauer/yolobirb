
function updateInfo(status) {
    //    alert(status);
    if (status == 'Success')
        window.location = '/';
}

function main() {



//alert($.QueryString.login);
//$.QueryString.login = 'login1';
//history.pushState(null, '', "?" + $.param($.QueryString));

    var postForm = {
        data: function () {
            return {
                post:
                {
                    title: 'default', text: ''
                }
            };
        },
        template:
            `
        <span>
        Write a post, if you want:
        <br>
        <textarea rows=20 cols=50 v-model='post.text'>text</textarea>
        <br>
        <button class='button' @click='submitRedirect'>Post</button>
        </span>
        `,
        methods:
        {
            submitRedirect: function () 
            {
                $.post('/api/add_post', JSON.stringify({post: this.post, login: $.QueryString.login}), this.postCheck);
            },
            postCheck: function (status) {
                this.$emit('postCreated');
            }
        }
    }

    var authorLink =
    {
        props: ['author'],
        template:
        `
        <a @click='goToAuthor' :href="'user/?login=' + author">@{{author}}</a>
        `,
        methods:
        {
            goToAuthor: function()
            {
                event.preventDefault()
                this.$emit('goToAuthor', this.author)
            }
        }
    }

    var wallPosts =
    {
        props: ['posts'],
        components:
        {
            'author-link': authorLink
        },
        template:
            `
        <span>
        
        <div v-for='post in posts'>
        
            <div class='post'>
            <div class='text'>
                
            Posted at {{post.time}} by <author-link @goToAuthor='doSmth' :author='post.author'></author-link>
                {{post.title}}
                <div class='text'>{{post.text}}
                <br></div>
            
                
                </div>

            </div>

        </div>
        </span>
        `,
        methods:
        {
            doSmth: function(sad)
            {
                this.$emit('goToAuthor', sad)
            }
        }
    }

    var wallController =
    {
        template:
            `
        <span>
        
        <button class='button' @click="$emit('show-more')">Show more</button>
        <button class='button' @click="$emit('show-less')">Show less</button>
        <button class='button' @click="$emit('go-prev')">Prev</button>
        <button class='button' @click="$emit('go-next')">Next</button>

        </span>
        `
    }

    var logoutButton = {
        template:
            `
        <button class='button' @click='logout'>Logout</button>
        `,
        methods:
        {
            logout: function () {
                $.get('/api/logout');
                window.location = '/';
            }
        }
    }

    var wall = {
        data: function () {
            return { posts: [{ time: '', author: '', title: '', text: '' }], control: { offset: 0, max: 5 } };
        },
        components:
        {
            'wall-posts': wallPosts,
            'wall-controller': wallController,
            'post-form': postForm
        },
        methods:
        {
            loadPosts: function () {
                //$.post('/api/get_posts', JSON.stringify({control: this.control,login: $.QueryString.login}), posts => this.posts = posts);  //todo change to get query
                $.ajax({
                    url: '/api/get_posts', 
                    data: JSON.stringify({control: this.control,login: $.QueryString.login}),
                    method: 'post',
                    error: function(XMLHttpRequest, textStatus, errorThrown){
                        //alert('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
                        alert('There is no user with that username')
                    },
                    success: posts => this.posts = posts
                })
            },
            showMore: function () {

                this.control.max = 20;
                this.loadPosts();
            },
            showLess: function () {
                this.control.max = 5;
                this.loadPosts();
            },
            goNext: function () {
                if (this.posts.length < this.control.max)
                    return;
                this.control.offset += this.control.max;
                this.loadPosts();
            },
            goPrev: function () {
                this.control.offset -= this.control.max;
                if (this.control.offset < 0)
                    this.control.offset = 0;
                this.loadPosts();
            },
            goToAuthor: function(login){
            $.QueryString.login = login;
            history.pushState(null, '', "?" + $.param($.QueryString));
            this.control.offset = 0
            this.control.max = 5
            this.loadPosts()
            }
        },
        template:
        `
        <span>
        
        <div class="child">
        <post-form @postCreated='loadPosts'></post-form>
        </div>

        <div class="child">
        <wall-controller @go-next='goNext'  @show-more='showMore' @show-less='showLess' @go-prev='goPrev'></wall-controller>
        <wall-posts :posts='posts' @goToAuthor='goToAuthor'></wall-posts>
        </div>

        </span>
        `,
        created: function () {
            this.loadPosts();
        }
    }

    new Vue({
        el: '#logout-button',
        components: { logout: logoutButton }
    })

    new Vue({
        el: '#wall',
        components: { wall: wall }
    })

    //vm.loadPosts();

}
$(document).ready(main);