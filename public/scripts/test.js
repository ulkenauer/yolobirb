
function main() {

    var example1 = new Vue({
        el: '#example-1',
        data: {
            counter: 0
        }
    })

    Vue.component('button-counter', {
        data: function () {
            return {
                count: 0
            }
        },
        template:
            `<button v-on:click="count++">
            Счётчик кликов — {{ count }}
            </button>`
    })

    new Vue({el: '#components-demo'})

}
$(document).ready(main);